<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ItemController@index')->name('qr');

Route::group([
        'prefix' => 'api',
    ], function() {

    Route::group([
        'prefix' => 'item',
    ], function() {

        Route::get('getItem/{code}', 'ItemController@getItem');
	});
	
	Route::group([
        'prefix' => 'trans',
    ], function() {
        Route::post('insert', 'TransactionController@insert_data');
	});
	
});

Route::group([
        'prefix' => 'trans',
    ], function() {

        Route::get('get_barcode/{code}', 'TransactionController@get_barcode');
});
	