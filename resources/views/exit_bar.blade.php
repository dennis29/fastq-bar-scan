<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FastQ</title>
     	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    </head>
    <body>
	<meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="container" id="QR-Code">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="navbar-form navbar-left">
                        <h4>FastQ</h4>
                    </div>
                </div>
                <div class="panel-body text-center">
            		    <div class="text-center">
							<span class = "glyphicon glyphicon-download"></span><span class = "glyphicon glyphicon-download"></span>&nbsp;&nbsp;Download and use this struct to exit:&nbsp;
							<span class = "glyphicon glyphicon-download"></span><span class = "glyphicon glyphicon-download"></span>
							<br/><a download="{{$data}}.jpg" href="{{asset('gbr/')}}/{{$data}}.jpg" title="{{$data}}.jpg">
							<img alt="ImageName" src="{{asset('gbr/')}}/{{$data}}.jpg"/></a>
						</div>
                </div>
            </div>
        </div>
       	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-confirm.min.js') }}"></script>
    </body>
</html>