<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FastQ</title>
     	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    </head>
    <body>
	<meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="container" id="QR-Code">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="navbar-form navbar-left">
                        <h4>FastQ</h4>
                    </div>
                    <div class="navbar-form navbar-right">
                        <select class="form-control" id="camera-select"></select>
                        <div class="form-group">
                            <input id="image-url" type="text" class="form-control" placeholder="Image url">
                            <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                            <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                            <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                            <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                            <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                         </div>
                    </div>
                </div>
                <div class="panel-body text-center">
                    <div class="col-md-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                        <div class="well" style="width: 100%;">
                            <label id="zoom-value" width="100">Zoom: 2</label>
                            <input id="zoom" onchange="Page.changeZoom();" type="range" min="10" max="30" value="20">
                            <label id="brightness-value" width="100">Brightness: 0</label>
                            <input id="brightness" onchange="Page.changeBrightness();" type="range" min="0" max="128" value="0">
                            <label id="contrast-value" width="100">Contrast: 0</label>
                            <input id="contrast" onchange="Page.changeContrast();" type="range" min="0" max="64" value="0">
                            <label id="threshold-value" width="100">Threshold: 0</label>
                            <input id="threshold" onchange="Page.changeThreshold();" type="range" min="0" max="512" value="0">
                            <label id="sharpness-value" width="100">Sharpness: off</label>
                            <input id="sharpness" onchange="Page.changeSharpness();" type="checkbox">
                            <label id="grayscale-value" width="100">grayscale: off</label>
                            <input id="grayscale" onchange="Page.changeGrayscale();" type="checkbox">
                            <br>
                            <label id="flipVertical-value" width="100">Flip Vertical: off</label>
                            <input id="flipVertical" onchange="Page.changeVertical();" type="checkbox">
                            <label id="flipHorizontal-value" width="100">Flip Horizontal: off</label>
                            <input id="flipHorizontal" onchange="Page.changeHorizontal();" type="checkbox">
                        </div>
                    </div>
                    <div class="col-md-6" style="display:none">
                        <div id="result">
                            <div >
                                <img id="scanned-img" src="" >
                            </div>
                            <div>
                                <label id="scanned-QR"></label>
		                   </div>
                        </div>
                    </div>
					<div class="col-md-6">
					   	<div id="img_src"></div>
					</div>
					<div class="col-md-6">
                        <div class="thumbnail" id="result">
                            <div class="caption">
            					<table class="table table-bordered" id="table">
								  <thead>
									<tr>
									  <th scope="col">No</th>
									  <th scope="col">Item Code</th>
									  <th scope="col">Item Name</th>
									  <th scope="col">Item Price</th>
									</tr>
								  </thead>
								  <tbody>
								  </tbody>
								  <tfoot>
									<tr>
									 <th colspan=3 style="text-align:right">Total Price : </th>
									 <th id="footer" style="text-align:right"></th>
									 <input type="hidden" id="tot_hidden" value=0>
									</tr>
								  </tfoot>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/filereader.js"></script>
        <!-- Using jquery version: -->
        <!--
            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/qrcodelib.js"></script>
            <script type="text/javascript" src="js/webcodecamjquery.js"></script>
            <script type="text/javascript" src="js/mainjquery.js"></script>
        -->

		<script type="text/javascript" src="{{ asset('js/qrcodelib.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/webcodecamjs.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-confirm.min.js') }}"></script>
		
		<!--
		<div class="text-center">
		  <button type="button" id="add" style='margin-bottom:50px'  class="btn btn-success btn-lg">Add</button>
		  <button type="button" id="save_trans" style='margin-bottom:50px'  class="btn btn-success btn-lg">Save Transaction</button>
		</div>
		-->
		<div class="text-center" id="payment-gateway" style="display: none;">
		   <div id="paypal-button-container"></div>

			<!-- Include the PayPal JavaScript SDK -->
			<script src="https://www.paypal.com/sdk/js?client-id=AWEnSxOY6JN7NBXveb4wV1hhlvghgJixOddbAazRJbIlzS5zSG_M7pW9sDHOg7KjgEnhtBqjh1iIF4h7&currency=USD"></script>
		    <script>
			paypal.Buttons({

					// Set up the transaction
					createOrder: function(data, actions) {
						return actions.order.create({
							purchase_units: [{
								amount: {
									value: $('#tot_hidden').val()
								}
							}]
						});
					},

					// Finalize the transaction
					onApprove: function(data, actions) {
						return actions.order.capture().then(function(details) {
							// Show a success message to the buyer
							alert('Transaction completed by ' + details.payer.name.given_name + '!');
							 save_item();
						});
					}


				}).render('#paypal-button-container');

			</script>
		</div>
		
    </body>
</html>
<script>  
var arrx = [];
var tot = 0;

function add_item(str=""){
	var item = ($("#scanned-QR").text()).split(":");
		  $.ajax({
		      async: false,
			  type: "GET",
			  //url: "api/item/getItem/"+item[1].replace(" ",""),
			  url: "api/item/getItem/AB12345678910",
			  success: function(data){
			     if(data != "")
				 {
					 arrx.push(data);
					 $('#scanned-img').attr('src','');
					 $("#scanned-QR").text("");
				 } 
			  }
		  });
		  if(arrx != "")
		  {  
	          $("#payment-gateway").attr('style',"display:block");
			  var i = 1;
			  var table = "";
			  $('#table > tbody').html("");
			  $.each(arrx, function( index, value ) {
				 table +=  "<tr><th>"+i+"</th><th>"+value.item_code+"</th>";
				 table +=  "<th>"+value.item_name+"</th><th style='text-align:right'>"+formatIdr(value.item_price)+"</th></tr>"; 
				 i++;
				 tot += value.item_price;			 
			  });
			  
			  $('#qty').val(1);
			  $('#footer').html(formatIdr(tot));
			  $('#table > tbody').html(table);
			  $('#payment-gateway').val(tot);
			  
			  $('#tot_hidden').val(tot)
				
			  tot = 0;
			 
		  }
}
function save_item(){
    		$.ajax({
								  async: false,
								  headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								  },
								  type: "POST",
								  url: "api/trans/insert/",
								  data: "input="+JSON.stringify(arrx)+"&buyer=dennis&tot="+$('#tot_hidden').val(), 
								  success: function(data){
									 if(data != "")
									 {
										 window.location.href = 'trans/get_barcode/'+data;
									 } 
								  }
		});	
	
}
	  
	  $('#save_trans').on('click', function() {
         save_item();
	  })	  
	  
	  $('#add').on('click', function() {
		  
		add_item("1:AB12345678910");
		  
	  })
	  
	  function formatIdr(angka="")
	  {
	     var reverse = angka.toString().split('').reverse().join(''),
		 ribuan = reverse.match(/\d{1,3}/g);
	     return ribuan.join('.').split('').reverse().join('');
	  }
</script>