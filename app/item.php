<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item extends Model
{
    protected $fillable = [
        'item_code', 'item_name' , 'item_price' , 'created_at' , 'updated_at'
    ];
}
