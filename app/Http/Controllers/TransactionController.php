<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\transaction;

use Intervention\Image\ImageManagerStatic as Image;

use Milon\Barcode\DNS2D;


class TransactionController extends Controller
{
   function insert_data(Request $request)
   {
		//dd(json_decode($request->input('input'), true));
		
		$data = json_decode($request->input('input'), true);
		
		$input_id  = "";
		
		foreach($data as $v)
		{
		  $input_id .= $v['id'].",";
		}
		
		$buyer = $request->input('buyer');
		$tot = $request->input('tot');
		
		//echo substr($input_id, 0, -1);
		unset($request);
		$request['buyer'] = $buyer;
		$request['item_id'] = substr($input_id, 0, -1);
		$request['tot'] = $tot;
		$get = transaction::create($request);
		/*
		$transaction = new Transaction;
		$transaction->buyer = $request->input('buyer');
		$transaction->tot = $request->input('tot');
		$transaction->item_id = substr($input_id, 0, -1);
		$get = $transaction->save();
		*/
		
		//$save = Transaction::create(array('buyer'=>$request->input('buyer'),'item_id'=>$input_id,'tot'=>$request->input('tot')));
	    //$id_trans = Product::create($request->all());
	    //return response()->json(Item::where('item_code','=',$code)->first());
		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		//return response()->json('<img src="data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('Ymd'), $generator::TYPE_CODE_128)).'">');
		
	
		// Output the image
		//imagejpeg('<img src="data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('Ymd'), $generator::TYPE_CODE_128)).'">'); 
		 $source = "EX".$get->id;
		 
		 $generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		 
		 $content = file_get_contents('data:image/jpg;base64,' . base64_encode($generator->getBarcode($source, $generator::TYPE_CODE_128)));
	     //$img = imagecreatefrompng('data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('YmdHis'), $generator::TYPE_CODE_128)));
		 
		 file_put_contents('gbr/'.$source.".jpg",$content);
		 
		 $img = Image::make(public_path('img/white.jpg'));  

         $img->text("EX".$get->id, 150, 100);  

         $img->save(public_path('gbr/txt'.$source.".jpg"));  
		 
		 $img = Image::make(public_path('gbr/txt'.$source.".jpg"));

         $img->insert(public_path('gbr/'.$source.".jpg"), 'top-center', -90, 60);

		 $img->save(public_path('gbr/'.$source.".jpg")); 
		 
		 unlink('gbr/txt'.$source.".jpg");
		
		 return response()->json($source);

    }
	
	function get_barcode($id="")
    {
	   return view('exit_bar')->with('data',$id);
    }

}
