<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\item;

class ItemController extends Controller
{
    public function index()
    {
	    return view('item');
    } 
	
	public function getItem($code="")
    {
	//	dd(Item::where('item_code','=',$code)->toSql());
	    return response()->json(Item::select('id','item_code','item_price','item_name')->where('item_code','=',$code)->first());
    }
	
	public function paypall()
    {
	  return view('welcome');
    }
}
