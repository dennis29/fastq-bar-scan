<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $fillable = [
	'buyer', 'item_id' , 'tot'
	];
}
